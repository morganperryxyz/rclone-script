#!/bin/sh 

# Directory where you mount rclone 
MOUNT_DIR="/mypool/jellyfin/media/whatbox" 
#String for rclone config 
RCLONE_MOUNT="whatbox:files/completed" 
MEDIA_DIR="/mypool/jellyfin/media" 

copy () { 
    # input is the directory name on external host 
    limit="5M" 
    currenttime=$(date +%H:%M) 
    if [ "$currenttime" > "23:00" ] || [ "$currenttime" < "06:30" ] 
    then 
            limit="10M" 
    fi 
    ls -d */ | fzf -m | xargs -I {} rclone copy --bwlimit $limit -vP $RCLONE_MOUNT/$1/{} "$MEDIA_DIR/$1/{}" && rclone rmdirs --leave-root $RCLONE_MOUNT/$1 
} 

case "$1" in 
    mov ) 
        cd "$MOUNT_DIR/mov" && copy mov mov 
        ;; 
    tv  ) 
        cd "$MOUNT_DIR/tv" && copy tv tv 
        ;; 
    mus ) 
        cd "$MOUNT_DIR/mus" && copy mus 
        ;; 
    audiobookz ) 
        cd "$MOUNT_DIR/audiobookz" && copy audiobookz 
        ;; 
    bookz ) 
        cd "$MOUNT_DIR/bookz" && copy bookz 
        ;; 
    *   ) 
        echo "Please use 'mov' or 'tv' or 'mus' or 'audiobookz' or 'bookz'" 
esac 
